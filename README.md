# Conversion script for OVA to QCOW2

Conversion script between single machine OVA/OVF and  libvirt-based virtual machine.



## Usage

The script must invoke with three parameters:

1. Name of the new virtual machine
2. Name of the target network
3. File OVA to convert