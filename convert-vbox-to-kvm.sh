#!/bin/bash

# Conversion script between single machine OVA/OVF and
# libvirt-based virtual machine.
# Copyright (C) 2020
# Andrea Giovine <andrea.giovine@studio.unibo.it>
# Davide Berardi <berardi.dav@gmail.com>
#	
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
#		
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#		
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

set -eu

filename() {
	echo "$1" | rev | cut -f 2- -d '.' | rev
}

cleanup() {
	rm -f *.ovf *.vmdk *.mf
}

unzip_ova() {
	tar -xvaf "$1" 2>/dev/null |
		grep -i 'disk'
}

convert_vmdk() {
	VM_NAME="$2"
	CONVERTED_DISKS=""
	# TODO Test multidisk
	for disk in $1; do
		echo "[ ] converting disk \"$disk\"" >&2
		TARGETDISK=$(echo "$disk" | sed 's/\.vmdk$/.qcow2/')
		TARGET="$VM_NAME-$TARGETDISK"
		CONVERTED_DISKS="$TARGET $CONVERTED_DISKS"
		if [ -e "$TARGET" ]; then
			continue
		fi
		qemu-img convert "$disk" "$TARGET" -O qcow2 -m "4" > /dev/null
	done
	echo "$CONVERTED_DISKS"
}

import_vm() {
	VM_NAME="$1"
	VM_NETWORK="$2"
	for disk in $3; do
		echo "[ ] building $VM_NAME with $disk"
		# Disk must be sata see bug
		# XXX link problem
		virt-install                                 \
			--name="$VM_NAME"                    \
			--vcpus=4                            \
			--memory=8192                        \
			--import                             \
			--disk "$disk,format=qcow2,bus=sata" \
			--os-variant=debian10                \
			--noautoconsole                      \
			--noreboot                           \
			--network network="$VM_NETWORK"
		# TODO Add multiple disks
	done
}

if [[ ! $# -eq 3 ]] ;then
	echo    "usage:" >&2
	echo -e "\t$0 <virtual machine name> <network> <ova file>" >&2
	echo    "example:" >&2
	echo -e "\t$0 vm-test-1 test-network test.ova" >&2
	exit 1
fi

VM_NAME="$1"
VM_NETWORK="$2"
F="$(filename $3)"

trap cleanup EXIT

DISKS="$(unzip_ova "$F.ova")"
TARGETS="$(convert_vmdk "$DISKS" "$VM_NAME")"

import_vm "$VM_NAME" "$VM_NETWORK" "$TARGETS"
